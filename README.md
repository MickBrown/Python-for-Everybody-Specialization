Python for Everybody Specialization: Getting started with Python & Python Data structures licensed by the University of Michigan. 

This project provides an overview of all the exercises.
Some code is borrowed from other course companions and adjusted accordingly.
All code is compatible with Python 3.

Happy coding! 

Kind regards, 
Mick Brown
